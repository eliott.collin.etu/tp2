import {Component} from "./Component.js";

export class Img extends Component {
    link;

    constructor(link) {
        super('img', {name: 'src', value: link}, undefined);
    }
}