export class Component {
    tagName;
    children;
    attribute;

    constructor(tagName, attribute, children) {
        this.tagName = tagName;
        this.attribute = attribute;
        this.children = children;
    }

    renderChildren(children) {
        if(children instanceof Array)
            return children.map(this.renderChildren).join('');

        if(children instanceof Component)
            return children.render();

        return children;
    }

    renderAttribute() {
        if(this.attribute) return `${this.attribute.name}="${this.attribute.value}"`;
        return "";
    }

    render() {

        if (this.children) {
            return `<${this.tagName} ${this.renderAttribute()}> ${this.renderChildren(this.children)} </${this.tagName}>`;
        } else {
            return `<${this.tagName} ${this.renderAttribute()}/>`;
        }
    }
}