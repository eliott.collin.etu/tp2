import {Component} from "../components/Component";
import {PizzaThumbnail} from "../components/PizzaThumbnail";

export class PizzaList extends Component {
    #pizzas;
    set pizzas(value) {
        console.log(value)
        this.#pizzas = value.map(pizza => new PizzaThumbnail(pizza));
    }

    constructor(pizzas) {
        super();
        this.#pizzas = pizzas
    }

    render() {
        return `<section class="pizzaList">
                    ${this.renderChildren(this.#pizzas)}
                </section>`;
    }

}