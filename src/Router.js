import {Component} from "./components/Component";

export class Router {
    static titleElement;
    static contentElement;
    static routes;

    static navigate(path) {
        const route = this.routes.find(value => value.path === path);

        this.titleElement.innerHTML = new Component('h1', null, route.title).render();
        this.contentElement.innerHTML = route.page.render();
    }
}